__Draw On Your Screen__
=======================
Start drawing with Super+Alt+D.
Then save your beautiful work by taking a screenshot.

![](https://framagit.org/abakkk/DrawOnYourScreen/raw/ressources/screenshot.jpg)

Features :
----------

* Basic shapes (rectangle, circle, ellipse, line, curve, text, free)
* Smooth stroke
* Drawing on desktop and persistence
* Multi-monitor support
* Export to SVG

Install :
----------

1. Download and decompress or clone the repository
2. Place the resulting directory in ~/.local/share/gnome-shell/extensions
3. IMPORTANT: change the directory name to drawOnYourScreen@abakkk.framagit.org
4. A small shot of `alt + F2` `r` to restart Gnome-shell under Xorg, restart or relogin under Wayland
5. Enable the extension in Gnome-tweak-tool
6. `Super + Alt + D` to test
7. [https://framagit.org/abakkk/DrawOnYourScreen/issues](https://framagit.org/abakkk/DrawOnYourScreen/issues) to say it doesn't work
