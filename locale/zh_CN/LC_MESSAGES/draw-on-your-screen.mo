��    W      �     �      �     �  �   �     c     i     �     �  �   �     a	  !   n	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     
     
     '
     F
     f
     ~
  0   �
     �
     �
     �
     �
                 
   2     =     B  	   O     Y     `     t     �     �     �     �     �  
   �     �     �        
     *        J     V  	   o     y     ~     �     �  
   �     �     �     �     �               "     .     ?     K  	   Z     d     k  (   �     �  R   �               !     &     9     X     d     �     �     �  
   �  5   �  �  �     �  �   �     �     �     �     �  �   �     �     �     �     �     �     �     �            
        !     (     /     <     O     b     u  -   |     �     �     �     �     �     �               "     )     6     =     M     a     n     �     �     �     �     �     �     �     �     �  !   �               8     ?     F     V     c          �     �     �     �     �     �     �     �               '     4     ;     K     j  A   �     �     �     �     �     �  .        H     h     o  !     	   �  3   �     %   .   M         E       !       N      ,   *           <   :                 1       V              "   4                                    	       3          6          R   S               G   =   $   F         /      '   U                    B       K      J   (   O   W              8   )       ?            @         Q   
          7           #       &   0       L   2   9           -          D   H   C   5   T               I   >                     A   P   ;   +    (in drawing mode) <u>Note</u>: When you save elements made with <b>eraser</b> in a <b>SVG</b> file,
they are colored with background color, transparent if it is disabled.
(See “%s” or edit the SVG file afterwards) About Add a drawing background Area screenshot Area screenshot to clipboard By pressing <b>Ctrl</b> key <b>during</b> the drawing process, you can:
 . rotate a rectangle or a text area
 . extend and rotate an ellipse
 . curve a line (cubic Bezier curve) Center click Change font family (generic name) Change font style Change font weight Change linecap Change linejoin Change the style Color Ctrl key Ctrl+1...9 Dashed Dashed line Decrement line width Decrement line width even more Disable on-screen notifications Disable panel indicator Draw Draw On Your Screen becomes Draw On Your Desktop Drawing on the desktop Ellipse Enter/leave drawing mode Entering drawing mode Erase Erase all drawings Erase last brushstroke Escape key Fill Free drawing Full line Global Hide panel and dock Increment line width Increment line width even more Increment/decrement line width Internal Leave Leaving drawing mode Left click Line Menu Open stylesheet.css Persistent Persistent drawing through session restart Preferences Press Ctrl + F1 for help Rectangle Redo Redo last brushstroke Right click Save drawing as a SVG file Screenshot Screenshot to clipboard Scroll See stylesheet.css Select color Select ellipse Select eraser Select line Select rectangle Select text Shift key held Show help Smooth Smooth last brushstroke Smooth stroke during the drawing process Square drawing area Start drawing with Super+Alt+D and save your beautiful work by taking a screenshot Stroke System Text Toggle fill/stroke Transform shape (when drawing) Translators Type your text
and press Enter Undo Undo last brushstroke Unselect shape (free drawing) Version %d You can also smooth the stroke afterward
See “%s” Project-Id-Version: Draw On Your Screen version 4
Report-Msgid-Bugs-To: https://framagit.org/abakkk/DrawOnYourScreen/issues/5
PO-Revision-Date: 2019-04-03 14:13+0800
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Last-Translator: Aerowolf <aerowolf@tom.com>
Plural-Forms: nplurals=1; plural=0;
Language: zh_CN
 （在绘画模式中有效） <u>注意</u>：当您保存<b>SVG</b>文件时，如果其中有<b>橡皮擦</b>生成的元素（擦除形成的轨迹），它们将被以背
景色着色。如果背景色被禁用，则显示为透明。（请参见“%s”或事后编辑SVG文件） 关于 添加绘画背景 区域截图 截取区域到剪切板 在绘画<b>期间</b>按<b>Ctrl</b>键，您可以：
 . 旋转矩形或文字区域
 . 拉伸和旋转圆形
 . 平滑线条（三次贝塞尔曲线） 点击中键 修改字体（通用名称） 修改字体样式 修改字体大小 修改端点样式 修改笔锋形状 修改样式 色彩 Ctrl键 Ctrl+1...9 虚线 虚线 减小线宽 大幅减小线宽 禁止屏幕提示 禁止托盘图标 绘画 从在屏幕上绘画改为在桌面上绘画 在桌面上绘画 圆形 进入/离开绘画模式 进入绘画模式 擦除 擦除所有绘画 擦除上一笔 Esc键 色块 随意绘画 实线 全局快捷键 隐藏面板和Dock 增加线宽 大幅增加线宽 增加/减小线宽 内部快捷键 离开 离开绘画模式 点击左键 直线 菜单 打开stylesheet.css 持续 在进程重启期间中断绘画 参数 按Ctrl + F1获取帮助 矩形 重做 重画上一笔 点击右键 将绘画保存为SVG文件 屏幕截图 截取屏幕到剪切板 滚动中键 参见stylesheet.css 选择色彩 选择圆形 选择橡皮擦 选择直线 选择矩形 选择文字 按住Shift键 显示帮助 平滑 平滑上一笔 在绘画过程中平滑笔划 将画布修整为正方形 按Super+Alt+D键开始绘画，以屏幕截图保存您的作品 笔划 系统快捷键 文字 切换色块/线条 改变形状（绘画期间） <a href="mailto:aerowolf@tom.com">Aerowolf</a> 输入文字
然后按回车键 撤消 撤消上一笔 不选择形状（随意绘画） 版本 %d 您也可以事后再平滑笔划
请参见“%s” 