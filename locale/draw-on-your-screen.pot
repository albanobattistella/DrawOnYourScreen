# <language> translations for Draw On Your Screen.
# Copyright (C) 2019 Listed translators
#
# This file is distributed under the same license as Draw On Your Screen.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Draw On Your Screen VERSION\n"
"Report-Msgid-Bugs-To: https://framagit.org/abakkk/DrawOnYourScreen/issues\n"
"POT-Creation-Date: 2019-03-04 16:40+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

# add your name here, for example:
# "Aïssata\n"
# "<a href=\"mailto:ali@mail.org\">Ali</a>\n"
# "<a href=\"https://...\">丽</a>"
# It will be displayed in About page
msgid "Translators"
msgstr ""

#: extension.js
msgid "Leaving drawing mode"
msgstr ""

msgid "Press Ctrl + F1 for help"
msgstr ""

msgid "Entering drawing mode"
msgstr ""

#: draw.js
msgid "Free drawing"
msgstr ""

msgid "Line"
msgstr ""

msgid "Ellipse"
msgstr ""

msgid "Rectangle"
msgstr ""

msgid "Text"
msgstr ""

msgid "Fill"
msgstr ""

msgid "Stroke"
msgstr ""

msgid "Dashed line"
msgstr ""

msgid "Full line"
msgstr ""

msgid ""
"Type your text\n"
"and press Enter"
msgstr ""

msgid "Screenshot"
msgstr ""

msgid "Screenshot to clipboard"
msgstr ""

msgid "Area screenshot"
msgstr ""

msgid "Area screenshot to clipboard"
msgstr ""

msgid "System"
msgstr ""

msgid "Undo"
msgstr ""

msgid "Redo"
msgstr ""

msgid "Erase"
msgstr ""

msgid "Smooth"
msgstr ""

msgid "Dashed"
msgstr ""

msgid "Color"
msgstr ""

#: prefs.js

msgid "Preferences"
msgstr ""

msgid "About"
msgstr ""

# GLOBAL_KEYBINDINGS

msgid "Enter/leave drawing mode"
msgstr ""

msgid "Erase all drawings"
msgstr ""

# INTERNAL_KEYBINDINGS

msgid "Undo last brushstroke"
msgstr ""

msgid "Redo last brushstroke"
msgstr ""

msgid "Erase last brushstroke"
msgstr ""

msgid "Smooth last brushstroke"
msgstr ""

msgid "Select line"
msgstr ""

msgid "Select ellipse"
msgstr ""

msgid "Select rectangle"
msgstr ""

msgid "Select text"
msgstr ""

msgid "Unselect shape (free drawing)"
msgstr ""

msgid "Toggle fill/stroke"
msgstr ""

msgid "Increment line width"
msgstr ""

msgid "Decrement line width"
msgstr ""

msgid "Increment line width even more"
msgstr ""

msgid "Decrement line width even more"
msgstr ""

msgid "Change linejoin"
msgstr ""

msgid "Change linecap"
msgstr ""

# already in draw.js
#msgid "Dashed line"
#msgstr ""

msgid "Change font family (generic name)"
msgstr ""

msgid "Change font weight"
msgstr ""

msgid "Change font style"
msgstr ""

msgid "Hide panel and dock"
msgstr ""

msgid "Add a drawing background"
msgstr ""

msgid "Square drawing area"
msgstr ""

msgid "Save drawing as a SVG file"
msgstr ""

msgid "Open stylesheet.css"
msgstr ""

msgid "Show help"
msgstr ""

# OTHER_SHORTCUTS

msgid "Draw"
msgstr ""

msgid "Left click"
msgstr ""

msgid "Menu"
msgstr ""

msgid "Right click"
msgstr ""

msgid "Center click"
msgstr ""

msgid "Transform shape (when drawing)"
msgstr ""

msgid "Ctrl key"
msgstr ""

msgid "Increment/decrement line width"
msgstr ""

msgid "Scroll"
msgstr ""

msgid "Select color"
msgstr ""

msgid "Ctrl+1...9"
msgstr ""

msgid "Select eraser"
msgstr ""

msgid "Shift key held"
msgstr ""

msgid "Leave"
msgstr ""

msgid "Escape key"
msgstr ""

# About page

# you are free to translate the extension name
#msgid "Draw On You Screen"
#msgstr ""

msgid "Version %d"
msgstr ""

msgid "Start drawing with Super+Alt+D and save your beautiful work by taking a screenshot"
msgstr ""

# Prefs page

msgid "Global"
msgstr ""

msgid "Drawing on the desktop"
msgstr ""

msgid "Draw On Your Screen becomes Draw On Your Desktop"
msgstr ""

msgid "Persistent"
msgstr ""

msgid "Persistent drawing through session restart"
msgstr ""

msgid "Disable on-screen notifications"
msgstr ""

msgid "Disable panel indicator"
msgstr ""

msgid "Internal"
msgstr ""

msgid "(in drawing mode)"
msgstr ""

msgid ""
"By pressing <b>Ctrl</b> key <b>during</b> the drawing process, you can:\n"
" . rotate a rectangle or a text area\n"
" . extend and rotate an ellipse\n"
" . curve a line (cubic Bezier curve)"
msgstr ""

msgid "Smooth stroke during the drawing process"
msgstr ""

msgid ""
"You can also smooth the stroke afterward\n"
"See “%s”"
msgstr ""

msgid "Change the style"
msgstr ""

msgid "See stylesheet.css"
msgstr ""

msgid ""
"<u>Note</u>: When you save elements made with <b>eraser</b> in a <b>SVG</b> file,\n"
"they are colored with background color, transparent if it is disabled.\n"
"(See “%s” or edit the SVG file afterwards)"
msgstr ""


# The following words refer to SVG attributes.
# You have the choice to translate or not

#msgid "Butt"
#msgstr ""

#msgid "Round"
#msgstr ""

#msgid "Square"
#msgstr ""

#msgid "Miter"
#msgstr ""

#msgid "Bevel"
#msgstr ""

#msgid "Normal"
#msgstr ""

#msgid "Bold"
#msgstr ""

#msgid "Italic"
#msgstr ""

#msgid "Oblique"
#msgstr ""

#msgid "Sans-Serif"
#msgstr ""

#msgid "Serif"
#msgstr ""

#msgid "Monospace"
#msgstr ""

#msgid "Cursive"
#msgstr ""

#msgid "Fantasy"
#msgstr ""

#msgid "px"
#msgstr ""

